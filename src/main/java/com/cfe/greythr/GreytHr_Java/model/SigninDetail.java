package com.cfe.greythr.GreytHr_Java.model;

import javax.persistence.*;

@Entity
public class SigninDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long signId;
    private Long inTime;
    private Long outTime;
    private Long date;

    @ManyToOne
    @JoinTable(name="user_signin", joinColumns = @JoinColumn(name="sign_id"),
               inverseJoinColumns = @JoinColumn(name="user_id"))
    private User user;

    public SigninDetail() {
    }

    public SigninDetail(Long inTime, Long outTime, Long date, User user) {
        this.inTime = inTime;
        this.outTime = outTime;
        this.date = date;
        this.user = user;
    }

    public Long getId() {
        return signId;
    }

    public Long getInTime() {
        return inTime;
    }

    public void setInTime(Long inTime) {
        this.inTime = inTime;
    }

    public Long getOutTime() {
        return outTime;
    }

    public void setOutTime(Long outTime) {
        this.outTime = outTime;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("SigninDetail{");
        sb.append("signId=").append(signId);
        sb.append(", inTime=").append(inTime);
        sb.append(", outTime=").append(outTime);
        sb.append(", date=").append(date);
        sb.append(", user=").append(user);
        sb.append('}');
        return sb.toString();
    }
}
