package com.cfe.greythr.GreytHr_Java;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GreytHrJavaApplication {

	public static void main(String[] args) {
		SpringApplication.run(GreytHrJavaApplication.class, args);
	}
}
