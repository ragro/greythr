package com.cfe.greythr.GreytHr_Java.repository;

import com.cfe.greythr.GreytHr_Java.model.SigninDetail;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SigninDetailRepository extends JpaRepository<SigninDetail, Long> {
}
