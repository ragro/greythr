package com.cfe.greythr.GreytHr_Java.repository;

import com.cfe.greythr.GreytHr_Java.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User,Long> {
}
