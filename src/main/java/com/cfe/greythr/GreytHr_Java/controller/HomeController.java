package com.cfe.greythr.GreytHr_Java.controller;

import com.cfe.greythr.GreytHr_Java.model.User;
import com.cfe.greythr.GreytHr_Java.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class HomeController {

    @Autowired
    UserService userService;

    @GetMapping("/home")
    public String getHome(Model model){
        model.addAttribute("user",new User());
        return "index";
    }

    @PostMapping("/signin")
    public String getSignIn(@ModelAttribute User user){
        userService.addUser(user);
        return "dashboard";
    }
}
