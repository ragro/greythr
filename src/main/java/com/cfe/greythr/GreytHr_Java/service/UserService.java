package com.cfe.greythr.GreytHr_Java.service;

import com.cfe.greythr.GreytHr_Java.model.User;

public interface UserService {
    public void addUser(User user);
}
