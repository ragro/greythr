package com.cfe.greythr.GreytHr_Java.service;

import com.cfe.greythr.GreytHr_Java.model.User;
import com.cfe.greythr.GreytHr_Java.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepo;

    public UserServiceImpl(UserRepository userRepo) {
        this.userRepo = userRepo;
    }

    @Override
    public void addUser(User user) {
        userRepo.save(user);
    }
}
